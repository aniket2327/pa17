#include<stdio.h>
int main()
{
    int i, n, arr[100], temp;
    int small = 9999;
    int large = -9999;
    int small_pos,large_pos;
    printf("enter the size of elements of array\n");
    scanf("%d", &n);
    for (i = 0; i < n; i++)
    {
        printf("enter the value of element %d\n", i);
        scanf("%d", &arr[i]);
    }
    for (i = 0; i < n; i++)
    {
        if (arr[i] < small)
        {
            small = arr[i];
            small_pos = i;
        }
        if (arr[i] > large)
        {
            large = arr[i];
            large_pos = i;
        }
    }
    printf("smallest no is %d\n", small);
    printf("position of smallest no is %d\n", small_pos);
    printf("largest no is %d \n", large);
    printf("position of largest no is %d\n", large_pos);
    temp = arr[large_pos];
    arr[large_pos] = arr[small_pos];
    arr[small_pos] = temp;
    printf("new array is\n");
    for (i = 0; i < n; i++)
        printf("arr[%d]=%d\n", i, arr[i]);
    return 0;
}
